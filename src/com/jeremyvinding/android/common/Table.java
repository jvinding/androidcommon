/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.android.common;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Table {
    public String getTableName() {
        return mTableName;
    }

    public String getCreateStatement() {
        return mCreateStatement;
    }

    public List<String> getIndices() {
        return mIndices;
    }

    public static class Builder {
        static Builder newInstance(final String tableName) {
            return new Builder(tableName);
        }

        Builder addIntegerPrimaryKey(final String name, final boolean isAutoIncrement) {
            addColumn(name, isAutoIncrement ? Type.INTEGER_PRIMARY_KEY_AUTOINCREMENT : Type.INTEGER_PRIMARY_KEY);
            return this;
        }

        Builder addTextColumn(final String name, final boolean isNullable) {
            addColumn(name, isNullable ? Type.TEXT : Type.TEXT_NOT_NULL);
            return this;
        }

        Builder addIntegerColumn(final String name, final boolean isNullable) {
            addColumn(name, isNullable ? Type.INTEGER : Type.INTEGER_NOT_NULL);
            return this;
        }

        Builder addIntegerColumn(final String name, final int defaultValue) {
            addColumn(name, Type.INTEGER_NOT_NULL + " DEFAULT " + defaultValue);
            return this;
        }

        Builder addBooleanColumn(final String name) {
            return addIntegerColumn(name, 0);
        }

        Builder addIndex(final String name, final String column) {
            mIndices.add(String.format("CREATE INDEX %s ON %s(%s)", name, mTableName, column));
            return this;
        }

        Table build() {
            return new Table(this);
        }

        private final String mTableName;
        private final LinkedHashMap<String, String> mColumns = new LinkedHashMap<String, String>();
        private final ArrayList<String> mIndices = new ArrayList<String>();

        private Builder(final String tableName) {
            mTableName = tableName;
        }

        private void addColumn(final String name, final String typeString) {
            if (mColumns.containsKey(name)) {
                throw new IllegalArgumentException("Column already exists: " + mTableName + "." + name);
            }
            mColumns.put(name, typeString);
        }

        private static class Type {
            static final String INTEGER_PRIMARY_KEY = " INTEGER PRIMARY KEY";
            static final String INTEGER_PRIMARY_KEY_AUTOINCREMENT = " INTEGER PRIMARY KEY AUTOINCREMENT";
            static final String TEXT = " TEXT";
            static final String TEXT_NOT_NULL = " TEXT NOT NULL";
            static final String INTEGER = " INTEGER";
            static final String INTEGER_NOT_NULL = " INTEGER NOT NULL";
        }
    }

    // Table Privates
    private final String mTableName;
    private final String mCreateStatement;
    private final ArrayList<String> mIndices;

    Table(final Builder builder) {
        mTableName = builder.mTableName;
        mIndices = builder.mIndices;
        final LinkedHashMap<String, String> columns = builder.mColumns;

        final StringBuilder createStatement = new StringBuilder("CREATE TABLE ")
                .append(mTableName)
                .append("(");

        boolean isFirstColumn = true;
        for (final String columnName : columns.keySet()) {
            if (isFirstColumn) {
                isFirstColumn = false;
            } else {
                createStatement.append(",");
            }
            createStatement.append(columnName).append(columns.get(columnName));
        }
        createStatement.append(")");
        mCreateStatement = createStatement.toString();
    }
}
