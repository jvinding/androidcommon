/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.android.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.util.List;

@SuppressWarnings("UnusedDeclaration")
public class AndroidUtils {
    public static boolean isIntentSupported(final Context context, final Intent intent) {
        final PackageManager mgr = context.getPackageManager();
        final List<ResolveInfo> list = mgr.queryIntentActivities(intent,
                                                                 PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    public static int getAppVersion(final Context context) {
        try {
            final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static float getDisplayScale(final Context context) {
        final DisplayMetrics dm = new DisplayMetrics();
        final WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(dm);
        return dm.scaledDensity;
    }

    public static boolean isOnline(final Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return null != netInfo && netInfo.isConnected();
    }

    @SuppressLint("NewApi")
    public static void dismissSoftKeyboard(final Activity activity) {
        final InputMethodManager manager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private AndroidUtils() {}
}
