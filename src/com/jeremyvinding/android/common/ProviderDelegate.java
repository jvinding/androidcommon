/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.android.common;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

interface ProviderDelegate {
    public String getType(Uri uri);

    public Cursor query(SQLiteDatabase db, Uri uri, String[] projection,
                        String selection, String[] selectionArgs, String sortOrder);

    public Uri insert(SQLiteDatabase db, Uri uri, ContentValues values);

    public int update(SQLiteDatabase db, Uri uri, ContentValues values, String selection, String[] selectionArgs);

    public int delete(SQLiteDatabase db, Uri uri, String selection, String[] selectionArgs);

    public String[] getSupportedPaths();
}
