/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.android.common;

import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;

@SuppressWarnings({"ConstantConditions", "UnusedDeclaration"})
public class LogUtils {
    public static final String TAG = "LogUtils";

    public static void LOGV(final String tag, final String msg, final Object... msgArgs) {
        LOGV(tag, msg, null, msgArgs);
    }

    public static void LOGV(final String tag, final String msg, final Throwable t, final Object... msgArgs) {
        final String cleanTag = getCleanTag(tag);
        if (BuildConfig.DEBUG && Log.isLoggable(cleanTag, Log.VERBOSE)) {
            try {
                Log.v(cleanTag, getMessage(msg, msgArgs, t));
            } catch (final Throwable throwable) {
                Log.e(TAG, "Error occurred while logging!", throwable);
            }
        }
    }

    public static void LOGD(final String tag, final String msg, final Object... msgArgs) {
        LOGD(tag, msg, null, msgArgs);
    }

    public static void LOGD(final String tag, final String msg, final Throwable t, final Object... msgArgs) {
        final String cleanTag = getCleanTag(tag);
        if (BuildConfig.DEBUG && Log.isLoggable(cleanTag, Log.DEBUG)) {
            try {
                Log.d(cleanTag, getMessage(msg, msgArgs, t));
            } catch (final Throwable throwable) {
                Log.e(TAG, "Error occurred while logging!", throwable);
            }
        }
    }

    public static void LOGI(final String tag, final String msg, final Object... msgArgs) {
        LOGI(tag, msg, null, msgArgs);
    }

    public static void LOGI(final String tag, final String msg, final Throwable t, final Object... msgArgs) {
        final String cleanTag = getCleanTag(tag);
        if (Log.isLoggable(cleanTag, Log.INFO)) {
            try {
                Log.i(cleanTag, getMessage(msg, msgArgs, t));
            } catch (final Throwable throwable) {
                Log.e(TAG, "Error occurred while logging!", throwable);
            }
        }
    }

    public static void LOGW(final String tag, final String msg, final Object... msgArgs) {
        LOGW(tag, msg, null, msgArgs);
    }

    public static void LOGW(final String tag, final String msg, final Throwable t, final Object... msgArgs) {
        final String cleanTag = getCleanTag(tag);
        if (Log.isLoggable(cleanTag, Log.WARN)) {
            try {
                Log.w(cleanTag, getMessage(msg, msgArgs, t));
            } catch (final Throwable throwable) {
                Log.e(TAG, "Error occurred while logging!", throwable);
            }
        }
    }

    public static void LOGE(final String tag, final String msg, final Object... msgArgs) {
        LOGE(tag, msg, null, msgArgs);
    }

    public static void LOGE(final String tag, final String msg, final Throwable t, final Object... msgArgs) {
        final String cleanTag = getCleanTag(tag);
        if (Log.isLoggable(cleanTag, Log.ERROR)) {
            try {
                Log.e(cleanTag, getMessage(msg, msgArgs, t));
            } catch (final Throwable throwable) {
                Log.e(TAG, "Error occurred while logging!", throwable);
            }
        }
    }

    private LogUtils() {
    }

    private static String getCleanTag(final String tag) {
        final String cleanTag;
        if (tag.length() > 23) {
            cleanTag = tag.substring(0, 23);
        } else {
            cleanTag = tag;
        }
        return cleanTag;
    }

    /**
     * Android logging won't log UnknownHostExceptions, but I wanna.
     *
     * @param throwable throwable to be logged
     * @return a pretty-ish string version of the stack trace
     */
    private static String getStackTraceString(final Throwable throwable) {
        final String stackTrace;
        if (null == throwable) {
            stackTrace = "";
        } else {
            final StringWriter sw = new StringWriter();
            throwable.printStackTrace(new PrintWriter(sw));
            stackTrace = sw.toString();
        }
        return stackTrace;
    }

    private static String getMessage(final String message, final Object[] args, final Throwable throwable) {
        final String formattedMessage;
        if (null == args || 0 == args.length) {
            formattedMessage = message;
        } else {
            formattedMessage = String.format(message, args);
        }
        return null == throwable ? formattedMessage : formattedMessage + '\n' + getStackTraceString(throwable);
    }
}
