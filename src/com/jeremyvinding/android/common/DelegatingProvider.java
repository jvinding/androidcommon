/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.android.common;

import android.annotation.SuppressLint;
import android.content.*;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.ArrayList;
import java.util.HashMap;

import static com.jeremyvinding.android.common.LogUtils.LOGV;

@SuppressWarnings("UnusedDeclaration")
public abstract class DelegatingProvider extends ContentProvider {
    protected abstract String getAuthority();

    protected abstract ProviderDelegate[] getDelegates();

    protected abstract SQLiteDatabase getWritableDatabase();

    protected abstract SQLiteDatabase getReadableDatabase();

    @Override
    public boolean onCreate() {
        final ProviderDelegate[] delegates = getDelegates();
        for (final ProviderDelegate delegate : delegates) {
            registerDelegate(delegate);
        }
        return true;
    }

    @Override
    public Cursor query(final Uri uri, final String[] projection, final String selection,
                        final String[] selectionArgs, final String sortOrder) {
        LOGV(TAG, "query(uri=%s)", uri);
        final Cursor cursor = getDelegate(uri).query(getReadableDatabase(),
                                                     uri,
                                                     projection,
                                                     selection,
                                                     selectionArgs,
                                                     sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(final Uri uri) {
        return getDelegate(uri).getType(uri);
    }

    @Override
    public Uri insert(final Uri uri, final ContentValues values) {
        LOGV(TAG, "insert(uri=%s, values=%s)", uri, values);
        final Uri insertUri = getDelegate(uri).insert(getWritableDatabase(), uri, values);
        notifyChange(uri, null);
        return insertUri;
    }

    @Override
    public int delete(final Uri uri, final String selection, final String[] selectionArgs) {
        LOGV(TAG, "delete(uri=%s)", uri);
        final int deleted = getDelegate(uri).delete(getWritableDatabase(), uri, selection, selectionArgs);
        if (0 != deleted) {
            LOGV(TAG, "deleted %d items", deleted);
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return deleted;
    }

    @Override
    public int update(final Uri uri, final ContentValues values, final String selection, final String[] selectionArgs) {
        LOGV(TAG, "update(uri=%s, values=%s)", uri, values);
        final int count = getDelegate(uri).update(getWritableDatabase(), uri, values, selection, selectionArgs);
        if (0 != count) {
            LOGV(TAG, "updated %d items", count);
            notifyChange(uri, null);
        }
        return count;
    }

    @SuppressLint("NewApi")
    @Override
    public ContentProviderResult[] applyBatch(final ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        final SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                final ContentProviderOperation operation = operations.get(i);
                results[i] = operation.apply(this, results, i);
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }

    public void notifyChange(final Uri uri, final ContentObserver observer) {
        getContext().getContentResolver().notifyChange(uri, observer);
    }

    // Privates
    private static final String TAG = DelegatingProvider.class.getSimpleName();
    private final String authority = getAuthority();
    private final ArrayList<ProviderDelegate> mDelegates = new ArrayList<ProviderDelegate>();
    private final HashMap<String, Integer> mPathCodes = new HashMap<String, Integer>();
    private final UriMatcher mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private ProviderDelegate getDelegate(final Uri uri) {
        final int code = mUriMatcher.match(uri);
        if (code < 0) {
            throw new IllegalStateException("unsupported uri: " + uri);
        }
        return mDelegates.get(code);
    }

    private void registerDelegate(final ProviderDelegate delegate) {
        final String[] paths = delegate.getSupportedPaths();
        for (final String path : paths) {
            if (mPathCodes.containsKey(path)) {
                throw new IllegalStateException("url already registered: " + path);
            }
            final int code = mDelegates.size();
            mUriMatcher.addURI(authority, path, code);
            mPathCodes.put(path, code);
            mDelegates.add(delegate);
        }
    }
}
